# Use the official Corda base image
FROM corda/corda-enterprise-alpine-zulu-java1.8-4.7.10

# Switch to root user to create directories and copy files
USER root

# Install SSH server (OpenSSH in this example)
RUN apk update \
    && apk add --no-cache openssh-server \
    && rm -rf /var/cache/apk/*

# Create a directory for SSHD's runtime files
RUN mkdir /var/run/sshd

# Switch back to the corda user for running the Corda node
USER corda

# Set the working directory
WORKDIR /opt/corda/node

# Create directories for notary, partya, and partyb
RUN mkdir -p notary \
    && mkdir -p partya \
    && mkdir -p partyb

# Copy the node.conf files to the respective directories in the container
COPY notary/node.conf /opt/corda/node/notary/
COPY partya/node.conf /opt/corda/node/partya/
COPY partyb/node.conf /opt/corda/node/partyb/

# Copy the Corda JAR file to the container
COPY corda-tools-network-bootstrapper-4.9.jar /opt/corda/

# Create a directory for Cordapps
# RUN mkdir /opt/corda/cordapps

# Copy the Cordapp projects to the container
# COPY cordapp /opt/corda/cordapps

# Expose the P2P, RPC, and SSH ports (adjust the ports as needed)
EXPOSE 10002 10003 2222

# Set the environment variable to accept the license
ENV ACCEPT_LICENSE=Y

# Start the Corda node with the given command-line arguments
CMD ["java", "-jar", "/opt/corda/corda-tools-network-bootstrapper-4.9.jar", "--dir", "/opt/corda/node", "--verbose"]

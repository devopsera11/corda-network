# corda-network

Docker image:


create Docker image for running a Corda 4.9 node.

The node can be bootstrapped using the “network-boostrapper” tool, here you can find documentation and instructions on how to use it:

https://docs.r3.com/en/platform/corda/4.9/enterprise/network-bootstrapper.html#test-deployments

 

The output of the network bootstrapper is a node that can be then containerised.
 

Docker Compose File:

create a docker-compose.yml file that can bring up a Corda node. The docker-compose.yml file should include the following:

 

A service that runs the Corda node image.
A Postgres Database to which the Node will connect to.
 

Helm Chart File:

create a Helm chart file that can be used to deploy the Corda node image built previously to a Kubernetes instance. The Helm chart file should include the following:

 

    A Kubernetes deployment for the Corda node.

    Any additional resources required to support the Corda node, such as a database or network service.

 

Terraform File:

create a Terraform file that deploys a Kubernetes instance in Azure, a Postgres database, creates a rule so that the AKS cluster can connect to the database, and deploys the Helm chart file created in the previous section. The Terraform file should include the following:

 

    A Kubernetes deployment (AKS).

    A database deployment that is compatible with the Corda node.

    A network rule that allows the Kubernetes instance to connect to the database.

 

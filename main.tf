# Provider configuration for Azure
provider "azurerm" {
  features {}
}

# Resource Group
resource "azurerm_resource_group" "aks_rg" {
  name     = "corda-aks-rg"
  location = "East US"
}

# Azure Kubernetes Service (AKS)
resource "azurerm_kubernetes_cluster" "aks" {
  name                = "corda-aks-cluster"
  location            = azurerm_resource_group.aks_rg.location
  resource_group_name = azurerm_resource_group.aks_rg.name
  dns_prefix          = "corda-aks"

  default_node_pool {
    name       = "default"
    node_count = 3
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    environment = "dev"
  }
}

# Postgres Database
resource "azurerm_postgresql_server" "postgres" {
  name                = "corda-postgres"
  location            = azurerm_resource_group.aks_rg.location
  resource_group_name = azurerm_resource_group.aks_rg.name
  sku_name            = "GP_Gen5_2"
  storage_mb          = 5120
  version             = "12"

  administrator_login          = "postgresadmin"
  administrator_login_password = "P@ssw0rd123!" # Replace this with a secure password

  tags = {
    environment = "dev"
  }
}

# Postgres Firewall Rule to allow AKS cluster to connect
resource "azurerm_postgresql_firewall_rule" "aks_firewall_rule" {
  name                = "aks_firewall_rule"
  resource_group_name = azurerm_resource_group.aks_rg.name
  server_name         = azurerm_postgresql_server.postgres.name

  start_ip_address = azurerm_kubernetes_cluster.aks.kubelet_identity.*.node_public_ip_address[0]
  end_ip_address   = azurerm_kubernetes_cluster.aks.kubelet_identity.*.node_public_ip_address[0]
}

# Helm Chart Deployment
resource "helm_release" "corda_node" {
  name       = "corda-node"
  repository = "https://example.com/helm-repo" # Replace with the actual Helm repository URL
  chart      = "corda-node-chart"
  version    = "0.1.0"
  namespace  = "default"

  set {
    name  = "corda.image"
    value = "jimakosak/corda-node:latest"
  }

  set {
    name  = "corda.port"
    value = "10002"
  }

  # Add more set options as needed to customize the Helm chart values.
}
